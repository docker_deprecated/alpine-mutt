# alpine-mutt

#### [alpine-x64-mutt](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-mutt/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-mutt.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-mutt "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-mutt.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-mutt "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-mutt.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-mutt/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-mutt.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-mutt/)
#### [alpine-aarch64-mutt](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-mutt/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64/alpine-aarch64-mutt.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-mutt "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64/alpine-aarch64-mutt.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-mutt "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-mutt.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-mutt/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-mutt.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-mutt/)
#### [alpine-armhf-mutt](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-mutt/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhf/alpine-armhf-mutt.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-mutt "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhf/alpine-armhf-mutt.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-mutt "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-mutt.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-mutt/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-mutt.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-mutt/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [mutt](http://www.mutt.org/)
    - Mutt is a small but very powerful text-based mail client for Unix operating systems.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -i -t \
           -e RUN_USER_NAME={RUN_USER_NAME} \
           -e RUN_USER_UID={RUN_USER_UID} \
           -e RUN_USER_GID={RUN_USER_GID} \
           -e USER_NAME={USER_NAME} \
           -e USER_PASSWD={USER_PASSWD} \
           forumi0721alpinex64/alpine-x64-mutt:latest
```

* aarch64
```sh
docker run -i -t \
           -e RUN_USER_NAME={RUN_USER_NAME} \
           -e RUN_USER_UID={RUN_USER_UID} \
           -e RUN_USER_GID={RUN_USER_GID} \
           -e USER_NAME={USER_NAME} \
           -e USER_PASSWD={USER_PASSWD} \
           forumi0721alpineaarch64/alpine-aarch64-mutt:latest
```

* armhf
```sh
docker run -i -t \
           -e RUN_USER_NAME={RUN_USER_NAME} \
           -e RUN_USER_UID={RUN_USER_UID} \
           -e RUN_USER_GID={RUN_USER_GID} \
           -e USER_NAME={USER_NAME} \
           -e USER_PASSWD={USER_PASSWD} \
           forumi0721alpinearmhf/alpine-armhf-mutt:latest
```



----------------------------------------
#### Usage

* Run command
```
docker run -i -t \
           -e RUN_USER_NAME={RUN_USER_NAME} \
           -e RUN_USER_UID={RUN_USER_UID} \
           -e RUN_USER_GID={RUN_USER_GID} \
           -e USER_NAME={USER_NAME} \
           -e USER_PASSWD={USER_PASSWD} \
           forumi0721alpinex64/alpine-x64-mutt:latest
		   {TO_MAIL_ADDRESS} \
		   {MAIL_SUBJECT} \
		   {MAIL_CONTENTS}
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| USER_NAME          | Login username (default : forumi0721)            |
| USER_PASSWD        | Login password (default : passwd)                |
| USER_EPASSWD       | Login password (base64)                          |
| USER_UID           | Login user uid (default : 1000)                  |
| USER_GID           | Login user gid (default : 100)                   |



----------------------------------------
* [forumi0721alpinex64/alpine-x64-mutt](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-mutt/)
* [forumi0721alpineaarch64/alpine-aarch64-mutt](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-mutt/)
* [forumi0721alpinearmhf/alpine-armhf-mutt](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-mutt/)

